// @flow
import React from 'react';
import PropTypes from 'prop-types';



export default class SideNav extends React.Component {

    static LEFT : string = 'left';
    static RIGHT : string = 'right';

    props: {
        open: boolean,
        side: string,
        children?: React$Element<*>
    };

    constructor(props:any) {
        super(props);
    }

    showSideNav() :void {

    }

    hideSideNav() :void {

    }

    handleClick() :void {

    }

    handleContainerClick( event: any ) :void {

    }

    render() {
        return <aside>
            <div>{ this.props.children }</div>
        </aside>
    }
}

SideNav.propTypes = {
    open: PropTypes.bool,
    side: PropTypes.oneOf([SideNav.LEFT,SideNav.RIGHT]),
    children: PropTypes.node
};

<SideNav open={true} side="right"><div>k</div></SideNav>